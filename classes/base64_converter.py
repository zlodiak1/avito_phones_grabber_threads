from PIL import Image
import base64
import pytesseract
import io


class Base64Converter():
    def __init__(self):
        self.img_color_model = "RGB"
        self.img_color = (255, 255, 255)

    def get_text(self, show_phone_el):
        print('get_text', show_phone_el)
        phone_img_base64 = show_phone_el.get_attribute("src")
        phone_img_base64 = phone_img_base64.split('base64,')[-1].strip()
        pic = io.StringIO()
        image_string = io.BytesIO(base64.b64decode(phone_img_base64))
        image = Image.open(image_string)
        bg = Image.new(self.img_color_model, image.size, self.img_color)
        bg.paste(image, image)

        num = pytesseract.image_to_string(bg)
        return num
