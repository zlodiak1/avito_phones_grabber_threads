import datetime


class Logger:
    def __init__(self, error_log_filename, journal_path):
        self.error_log_filename = error_log_filename
        self.journal_path = journal_path         

    def get_curr_time(self):
        now = datetime.datetime.now()
        return now.strftime("%Y-%m-%d %H:%M")      

    def write_into_error_log(self, msg):
        with open(self.error_log_filename, 'a', encoding='utf-8') as f:
            full_msg = self.get_curr_time() + ' :: ' + msg + '\n'                
            f.write(full_msg)

    def write_into_journal(self, msg):
        with open(self.journal_path, 'a', encoding='utf-8') as f:
            full_msg = self.get_curr_time() + ' - ' + msg + '\n'                
            f.write(full_msg)