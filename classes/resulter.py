import datetime
import os
from multiprocessing import Lock


class Resulter:
    def __init__(self, results_path, logger):
        self.logger = logger
        now = datetime.datetime.now()
        curr_time = now.strftime("%Y-%m-%d %H:%M")           
        self.result_filename = os.path.join(results_path, curr_time + '.txt')
        self.remove_double_file()
        self.lock = Lock()

    def remove_double_file(self):
        try:
            os.remove(self.result_filename)
            self.logger.write_into_error_log('Resultfile double double has been deleted: ' + str(self.result_filename) + ' :: ' + str(e))
        except:
            print('No exist double file')

    def write_result(self, phone_num):
        with open(self.result_filename, 'a', encoding='utf-8') as f:           
            self.lock.acquire()
            try:
                f.write(phone_num + '\n') 
            except IOError as e:
                self.logger.write_into_error_log('Error write results.txt file: :: ' + str(e))
            finally:
                self.lock.release()
