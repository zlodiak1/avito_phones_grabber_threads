import requests

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, RemoteDriverServerException, ElementClickInterceptedException

class ImgRecognizer():
    def __init__(self, driver_path, delay_sec, selectors, base64_converter, link, logger, resulter):
        self.delay_sec = delay_sec
        self.hide_phone_selector = selectors['hide_phone_selector']
        self.show_phone_selector = selectors['show_phone_selector']
        self.base64_converter = base64_converter
        self.link = link
        self.logger = logger
        self.resulter = resulter
        self.driver = None
        self.driver_path = driver_path

    def show_phone(self, link):
        try:
            self.driver.get(link);
            hide_phone_el = self.driver.find_element_by_css_selector(self.hide_phone_selector)
            hide_phone_el.click()
        except RemoteDriverServerException as e:
            self.logger.write_into_error_log('Error driver get for link: ' + self.link + ' :: ' + str(e))
        except NoSuchElementException as e:
            self.logger.write_into_error_log('Error driver find element for link: ' + str(self.link) + ' :: ' + str(e))
        except ElementClickInterceptedException as e:
            self.logger.write_into_error_log('Error driver click for link: ' + self.link + ' :: ' + str(e))
        except Exception as e:
            self.logger.write_into_error_log('Error show phone for link: ' + self.link + ' :: ' + str(e))

    def recognize_img(self):
        try:
            show_phone_el = WebDriverWait(self.driver, self.delay_sec).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, self.show_phone_selector))               
            )
            # show_phone_el = EC.presence_of_element_located((By.CSS_SELECTOR, self.show_phone_selector))
            num = self.base64_converter.get_text(show_phone_el)
            return num
        except NoSuchElementException as e:
            self.logger.write_into_error_log('Error find css selector after click: ' + self.show_phone_selector + ' :: ' + str(e))
        except Exception as e:
            self.logger.write_into_error_log('Error recognize img after click: ' + self.show_phone_selector + ' :: ' + str(e))

    def run(self):
        self.driver = webdriver.Chrome(self.driver_path)
        try:
            self.show_phone(self.link)
            phone_num = self.recognize_img()
            print('phone_num:', phone_num)
            self.resulter.write_result(phone_num)
        except Exception as e:
            self.logger.write_into_error_log('Error get phone for link: ' + self.link + ' :: ' + str(e))
        finally:
            self.driver.close()

    def __del__(self):
        if self.driver:
            self.driver.close()
