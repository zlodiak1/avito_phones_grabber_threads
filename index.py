import config
from classes.img_recognizer import ImgRecognizer
from classes.links_retriever import LinksRetriever
from classes.base64_converter import Base64Converter
from classes.logger import Logger
from classes.resulter import Resulter

import os
from multiprocessing import Process


try:
    base64_converter = Base64Converter()
    links_retriever = LinksRetriever(config.query)
    links = links_retriever.get_links()

    curr_abs_dirname = os.path.dirname(os.path.abspath(__file__))
    error_log_path = os.path.join(curr_abs_dirname, config.error_log_file_name)
    results_path = os.path.join(curr_abs_dirname, config.results_catalog_name)
    journal_path = os.path.join(curr_abs_dirname, config.journal_filename)

    logger = Logger(error_log_path, journal_path)
    resulter = Resulter(results_path, logger)

    logger.write_into_journal(config.start_parse_phrase)




    for link in links:
        img_recognizer = ImgRecognizer(
            config.driver_path,
            config.delay_sec,
            {
                'hide_phone_selector': config.hide_phone_selector,
                'show_phone_selector': config.show_phone_selector
            },
            base64_converter,
            link, 
            logger,
            resulter
        )        
        proc = Process(target=img_recognizer.run)
        proc.start()



except Exception as e:
    print('GET NUMS IS FAILED :: ', e)
    logger.write_into_error_log('GET NUMS IS FAILED :: ' + str(e))